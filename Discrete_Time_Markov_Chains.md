**Markov Property** = The property of a random variables future is only dependent on the present

From the Markov Property, we can conclude that

$P[X_{n+1} = i_{n+1} | X_{0} = i_{0}, ... X_{n} = i_{n}] = P[X_{n+1} = i_{n+1} | X_{n} = i_{n}]$

## Representations

Each state can be represented as a node and the edges can represented as all possible states reach from that state.  The edge weights denote the probability of reaching a certain state from another state. The sum of edge weights, i.e. the sum of probabilities, should be equal to 1.

```mermaid
graph LR
0-- 1 -->1
1-- 0.5 -->0
1-- 0.5 -->2
2-- 0.5 -->1
2-- 0.5 -->3
3-- 0.5 -->2
3-- 0.5 -->4
4-- 0.5 -->3
4-- 0.5 -->5
5-- 0.5 -->4
5-- 0.5 -->6
6-- 1 -->5

```
*1: Example of a [Markov Chain](file:///Users/uni.haroun/Downloads/Ma222exqu13.pdf) with states {0, 1, 2, 3, 4, 5, 6}* 

If we can represent the states in a graph, then we can also represent the states in a **adjacency matrix**. However, we modify the matrix slightly. Instead of denoting 1 for node j being reachable from node i, we denote the probability of reaching node j from node i. This matrix is referred to as a **transition matrix**

$$
\begin{bmatrix}
0 & 1 & 0 & 0 & 0 & 0 & 0 \\
0.5 & 0 & 0.5 & 0 & 0 & 0 & 0 \\
0 & 0.5 & 0 & 0.5 & 0 & 0 & 0 \\
0 & 0 & 0.5 & 0 & 0.5 & 0 & 0 \\
0 & 0 & 0 & 0.5 & 0 & 0 & 0.5 \\
0 & 0 & 0 & 0 & 0 & 1 & 0 \\

\end{bmatrix}$$
*2: Transition matrix of the above Markov Chain (1)*

**Stationary/Equilibrium state** of a Markov Chain = A probability distribution of each state that does not change over time (I.E if we perform a random walk with an infinite amount of steps, what are is the probability distribution over each state)

Let A denote the transition matrix of a Markov Chain and let $\pi^0$ denote the initial probability distribution of each state. Then,
$$\pi^0A = \pi^1$$
$$\pi^1A = \pi^2$$
etc. If we have a $\pi^*$ such that $\pi^*A = \pi^*$ , then we say that $\pi*$ is the stationary/equilibrium distribution. Again, the sum of all elements in any $\pi^i$ must be equal to 1 because it is a probability distribution.

## Eigenvector and eigenvalue of the transition matrix
Finding the stationary state is equivalent to finding an eigenvector for transition matrix A such that the eigen value $\lambda$ = 1(???). From this, we can conclude that a transition matrix A, and thus the Markov Chain it represents, may have more than 1 stationary state.

*Check if this is correct:
Intuition: Eigenvector of a matrix is a vector such that when you apply the matrix transformation, the vector is only scaled. A stationary distribution tells you the overall state distribution. Thus, applying a vector with the state distribution to the transition matrix should not change the state distribution (since the state distribution is a distribution when performing an infinite random walk). In other words, the vector with the state distribution is unaffected by the matrix transformation, i.e =. scaled with 1.

**Transient State** = A type of state for which the probability of coming back to a state is less than 1 in a random walk.

**Recurrence State** = A type of state for which the probability of coming back to a state is equal to 1 in a random walk.

```mermaid
graph LR;
0-->1
1-->1
1-->2
2-->1
2-->2
```
*3: State 0 here denotes a Transient state since you can never return to state 0 after reaching state 1. State 1 and 2 denote recurrence states.

A Markov Chain is [reducible](https://link.springer.com/chapter/10.1007/978-3-319-15657-6_5#:~:text=Reducible%20Markov%20chains%20describe%20systems,we%20cannot%20visit%20other%20states)  if there exists a state such that we visit that state once, the state can not visited again. The reason for naming such a Markov Chain reducible, is that you can reduce this Markov Chain (split it up) into multiple smaller irreducible Markov Chains. In a **irreducible** Markov Chain, it is possible to visit every state, i.e. each state is **recurrent** (see definition of recurrence state). 

The probability of being in state $i$ and reaching state $j$, the **n-step transition probability** can be denoted as:
$p_{ij}^{(n)} = P[X_{n} = j | X_{0}= i]$

This can also be computed using a transition matrix:
$p_{ij}^{(n)} = A_{ij}^{n}$

The transition matrix encode with what probabilities you can go from one state to another. Therefore, multiplying the transition matrix with itself gives you the probabilities to from state to another state in n steps.

$p_{ij}^{(\infty)} = A_{ij}^{\infty} = stationary \space distribution$

This makes sense since reaching state j when performing infinite amount of steps should depend on the starting state i, but rather is a property of the whole Markov Chain.  I.e., reaching state j in an infinite amount of steps is the same as the overall probability of being in state j.